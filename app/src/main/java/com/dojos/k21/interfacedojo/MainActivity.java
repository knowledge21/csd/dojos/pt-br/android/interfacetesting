package com.dojos.k21.interfacedojo;

import android.content.Intent;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.NumberFormat;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.example.lula.myapplication.MESSAGE";
    private static final String CALCULADORA_URL = "http://172.18.0.1:8081/comissao";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void sendMessage(View view) throws JSONException {
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String valorVendaDigitado = editText.getText().toString();

        //Deixei fixo para nao precisar da API de pe
        String valorComissaoCalculado = getCommission(new BigDecimal(valorVendaDigitado)).toString();

        NumberFormat formatter = NumberFormat.getCurrencyInstance();
        String message = String.format("Para uma venda de %s o valor da comissão deve ser %s", valorVendaDigitado, valorComissaoCalculado);
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }

    public static BigDecimal getCommission(BigDecimal sale) {

        if (sale.doubleValue() > 10000) {
            return sale.multiply(new BigDecimal("0.06")).setScale(2, RoundingMode.FLOOR);
        }

        return sale.multiply(new BigDecimal(0.05)).setScale(2, RoundingMode.FLOOR);
    }
}
